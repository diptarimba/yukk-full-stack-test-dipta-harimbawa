<?php

namespace App\Http\Repository;

use App\Http\Repository\Interfaces\UserTransactionRepositoryInterface;
use App\Models\UserTransaction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserTransactionRepository implements UserTransactionRepositoryInterface
{
    public function history($id): Collection
    {
        $history = UserTransaction::with('type_transaction')->where('user_id', $id)->get();
        return $history;
    }

    public function find($id): Model
    {
        $user = UserTransaction::with('transaction_detail')->find($id);
        return $user;
    }

    public function approve_topup($id)
    {
        $transaction = $this->find($id);
        $transaction->transaction_detail()->update(['is_approve' => true]);
    }

    public function create($id, $attributes, $is_approve = false)
    {
        $create = UserTransaction::create([
            'user_id' => $id,
            'direction_user_id' => $attributes['direction_user_id'],
            'type_transaction_id' => $attributes['type_transaction_id'],
            'amount' => $attributes['amount'],
            'previous_balance' => $attributes['previous_balance'],
        ]);
        $create->transaction_detail()->create([
            'notes' => $attributes['notes'] ?? '',
            'proof' => $attributes['proof'] ?? '',
            'is_approve' => $is_approve,
        ]);
        return $create;
    }

    public function all_transaction_type($type, $is_approve = null, $search = '', $user_id = null)
    {
        $all_transaction = UserTransaction::
            when(!is_null($user_id), function($query) use ($user_id){
                $query->where('user_id', $user_id);
            })
            ->whereIn('type_transaction_id', $type)
            ->with('direction_user', 'type_transaction', 'user', 'transaction_detail')
            ->where(function ($query) use ($search, $is_approve) {
                $query->where('trx_id', 'ilike', '%' . $search . '%')
                    ->orWhere(function ($query) use ($search, $is_approve) {
                        $query->whereHas('transaction_detail', function ($query) use ($search) {
                            $query->where('notes', 'ilike', '%' . $search . '%');
                        })
                        ->orWhereHas('type_transaction', function ($query) use ($search) {
                            $query->where('name', 'ilike', '%' . $search . '%');
                        })
                        ->orWhereHas('user', function ($query) use ($search) {
                            $query->where('name', 'ilike', '%' . $search . '%');
                        })
                        ->orWhereHas('direction_user', function ($query) use ($search) {
                            $query->where('name', 'ilike', '%' . $search . '%');
                        });
                    })
                    ->when(!is_null($is_approve), function ($query) use ($is_approve) {
                        $query->whereHas('transaction_detail', function ($query) use ($is_approve) {
                            $query->where('is_approve', $is_approve);
                        });
                    });
            })
            ->orderBy('created_at', 'desc')
            ->select();
        return $all_transaction;
    }
}
