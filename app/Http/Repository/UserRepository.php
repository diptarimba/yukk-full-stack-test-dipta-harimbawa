<?php
namespace App\Http\Repository;

use App\Http\Repository\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRepository implements UserRepositoryInterface
{

    public function index(): Collection
    {
        $user = User::all();
        return $user;
    }

    public function find($id): Model
    {
        $user = User::findOrFail($id);
        return $user;
    }

    public function findByEmail($email): Model
    {
        $user = User::role('user')->where('email', $email)->first();
        return $user;
    }

    public function store($attributes): Model
    {
        $user = User::create($attributes);
        return $user;
    }

    public function update($id, $attributes): void
    {
        $user = User::findOrFail($id);
        $user->update($attributes);
    }

    public function modifyBalance($id, $amount, $operation = 'increment'): void
    {
        $user = User::findOrFail($id);
        $user->{$operation}('balance', $amount);
    }

    public function countAllUser(): int
    {
        $user = User::role('user')->count();
        return $user;
    }

    public function sumAllBalance(): int
    {
        $user = User::role('user')->sum('balance');
        return $user;
    }
}
