<?php
namespace App\Http\Repository\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

Interface TypeTransactionRepositoryInterface
{
    public function list(): Collection;
    public function find($code): Model;
    public function findSome($codes): Collection;
}
