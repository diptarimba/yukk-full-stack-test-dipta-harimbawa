<?php
namespace App\Http\Repository\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface UserTransactionRepositoryInterface
{
    public function find($id);
    public function approve_topup($id);
    public function all_transaction_type($type, $is_approve = null, $search = '', $user_id = null);
    public function history($id): Collection;
    public function create($id, $attributes);
}
