<?php
namespace App\Http\Repository\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface
{
    public function index(): Collection;
    public function find($id): Model;
    public function findByEmail($email): Model;
    public function countAllUser(): int;
    public function sumAllBalance(): int;
    public function store(array $attributes): Model;
    public function update($id, array $attributes): void;
    public function modifyBalance($id, int $attributes, string $operation): void;
}
