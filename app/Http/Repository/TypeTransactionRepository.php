<?php
namespace App\Http\Repository;

use App\Http\Repository\Interfaces\TypeTransactionRepositoryInterface;
use App\Models\TypeTransaction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class TypeTransactionRepository implements TypeTransactionRepositoryInterface
{
    public function list(): Collection
    {
        $typeTransaction = TypeTransaction::all();
        return $typeTransaction;
    }

    public function find($code): Model
    {
        $typeTransaction = TypeTransaction::where('code', $code)->first();
        return $typeTransaction;
    }

    public function findSome($codes): Collection
    {
        $typeTransaction = TypeTransaction::whereIn('code', $codes)->get();
        return $typeTransaction;
    }
}
