<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\User\Interfaces\UserControllerInterface;
use App\Http\Requests\User\UserTopupRequest;
use App\Http\Requests\User\UserTransferRequest;
use App\Http\Usecase\Interfaces\UserTransactionUsecaseInterface;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Ramsey\Uuid\Uuid;

class UserController extends Controller implements UserControllerInterface
{
    protected $userTransactionUsecase;
    public function __construct(UserTransactionUsecaseInterface $userTransactionUsecase)
    {
        $this->userTransactionUsecase = $userTransactionUsecase;
    }
    public function index()
    {
        $balance = $this->userTransactionUsecase->home();
        return view('page.user-dashboard.home', compact('balance'));
    }

    public function index_history()
    {
        return view('user.history');
    }

    public function topup_index(Request $request)
    {
        if($request->ajax()){
            return $this->userTransactionUsecase->topup_index($request);
        }
        return view('page.user-dashboard.topup-index');
    }

    public function topup()
    {
        return view('page.user-dashboard.topup');
    }
    public function topup_store(UserTopupRequest $attributes)
    {
        $this->userTransactionUsecase->topup(array_merge($attributes->all(), [
            'proof' => '/storage/' . $attributes->file('proof')->storePublicly('proof'),
            'direction_user_id' => '00000000-0000-0000-0000-000000000000'
        ]));
        return redirect()->route('user.topup.index')->with('success', 'Topup Success');
    }

    public function transfer_index(Request $request)
    {
        if($request->ajax()){
            return $this->userTransactionUsecase->transfer_index($request);
        }
        return view('page.user-dashboard.transfer-index');
    }
    public function transfer()
    {
        return view('page.user-dashboard.transfer');
    }
    public function transfer_store(UserTransferRequest $attributes)
    {
        $data = $this->userTransactionUsecase->transfer($attributes->all());
        if ($data['status'])
        {
            return redirect()->route('user.transfer.index')->with('success', $data['message']);
        } else {
            return redirect()->route('user.transfer.index')->with('error', $data['message']);
        }
    }
}
