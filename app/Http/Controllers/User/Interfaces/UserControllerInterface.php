<?php

namespace App\Http\Controllers\User\Interfaces;

use App\Http\Requests\User\UserTopupRequest;
use App\Http\Requests\User\UserTransferRequest;

interface UserControllerInterface
{
    public function index();
    public function index_history();
    public function topup();
    public function topup_store(UserTopupRequest $attributes);
    public function transfer();
    public function transfer_store(UserTransferRequest $attributes);
}
