<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Usecase\Interfaces\AdminTransactionUsecaseInterface;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    protected $adminTransactionUsecase;

    public function __construct(AdminTransactionUsecaseInterface $AdminTransactionUsecase)
    {
        $this->adminTransactionUsecase = $AdminTransactionUsecase;
    }
    public function index()
    {
        $data = $this->adminTransactionUsecase->index();
        return view('page.admin-dashboard.admin.home', compact('data'));
    }

    public function topup(Request $request)
    {
        if ($request->ajax()) {
            return $this->adminTransactionUsecase->topup($request);
        }
        return view('page.admin-dashboard.admin.topup');
    }

    public function topup_store($id)
    {
        $this->adminTransactionUsecase->topup_confirmed($id);
        return redirect()->route('admin.topup.index')->with('success', 'Topup Approved');
    }
}
