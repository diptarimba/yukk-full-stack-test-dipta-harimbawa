<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\StoreUserRequest;
use App\Http\Usecase\Interfaces\UserUsecaseInterface;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Inertia\Response;

class RegisteredUserController extends Controller
{
    protected $userUsecase;
    public function __construct(UserUsecaseInterface $userUsecase)
    {
        $this->userUsecase = $userUsecase;
    }
    /**
     * Display the registration view.
     */
    public function create()
    {

        return view('page.auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreUserRequest $request): RedirectResponse
    {

        $user = $this->userUsecase->store($request->all());
        $user->assignRole('user');
        event(new Registered($user));

        Auth::login($user);
        if ($request->user()->hasRole('admin')) {
            return redirect()->intended(route('admin.home', absolute: false));
        } else if ($request->user()->hasRole('user')) {
            return redirect()->intended(route('user.home', absolute: false));
        }

        return redirect(route('dashboard', absolute: false));
    }
}
