<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UpdatePasswordUserRequest;
use App\Http\Usecase\Interfaces\UserUsecaseInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class PasswordController extends Controller
{
    protected $userUsecase;
    public function __construct(UserUsecaseInterface $userUsecase)
    {
        $this->userUsecase = $userUsecase;
    }
    /**
     * Update the user's password.
     */
    public function update(UpdatePasswordUserRequest $request): RedirectResponse
    {
        $this->userUsecase->update($request->user()->id, ['password' => $request->password]);

        return back();
    }
}
