<?php
namespace App\Http\Usecase;

use App\Http\Repository\Interfaces\TypeTransactionRepositoryInterface;
use App\Http\Repository\Interfaces\UserRepositoryInterface;
use App\Http\Repository\Interfaces\UserTransactionRepositoryInterface;
use App\Http\Usecase\Interfaces\UserTransactionUsecaseInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserTransactionUsecase implements UserTransactionUsecaseInterface
{
    protected $userRepository;
    protected $userTransactionRepository;
    protected $typeTransactionRepository;
    protected $currentUser;
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserTransactionRepositoryInterface $userTransactionRepository,
        TypeTransactionRepositoryInterface $typeTransactionRepository)
    {
        $this->userRepository = $userRepository;
        $this->userTransactionRepository = $userTransactionRepository;
        $this->typeTransactionRepository = $typeTransactionRepository;
        $this->currentUser = Auth::user();
    }
    public function home()
    {
        return $this->userRepository->find($this->currentUser->id)->balance;
    }
    public function history()
    {
        return $this->userTransactionRepository->history($this->currentUser->id);
    }

    public function topup_index(Request $request)
    {
        $typeId = $this->typeTransactionRepository->find('topup')->id;
        $payload = $this->userTransactionRepository->all_transaction_type([$typeId], null, $request->search['value'], Auth::user()->id);
        return datatables()->of($payload)
            ->addIndexColumn()
            ->addColumn('amount', function($query){
                return "Rp. " . number_format($query->amount, 0, ',', '.');
            })
            ->addColumn('status', function ($data) {
                return $data->transaction_detail->is_approve ? 'Approve' : 'Pending';
            })
            ->make(true);
    }

    public function topup($attributes)
    {
        $typeId = $this->typeTransactionRepository->find('topup')->id;
        $this->userTransactionRepository->create($this->currentUser->id, array_merge($attributes, [
            'type_transaction_id' => $typeId,
            'previous_balance' => $this->currentUser->balance
        ]));
    }

    public function transfer_index(Request $request)
    {
        $types = $this->typeTransactionRepository->findSome(['transfer', 'receive']);
        $typeId = $types->pluck('id')->toArray();
        $payload = $this->userTransactionRepository->all_transaction_type($typeId, true, $request->search['value'], Auth::user()->id);
        return datatables()->of($payload)
            ->addIndexColumn()
            ->addColumn('status', function ($data) {
                return $data->transaction_detail->is_approve ? 'Approve' : 'Pending';
            })
            ->addColumn('amount', function($query){
                return "Rp. " . number_format($query->amount, 0, ',', '.');
            })
            ->addColumn('direction_user_name', function($query){
                return $query->type_transaction->conjunction . ' ' . $query->direction_user->name ?? '';
            })
            ->addColumn('created_at', function($query){
                return "{$query->created_at} ({$query->created_at->diffForHumans()})";
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function transfer($attributes)
    {
        $typeTransferId = $this->typeTransactionRepository->find('transfer')->id;
        $typeReceiveId = $this->typeTransactionRepository->find('receive')->id;
        $amountTransfer = $attributes['amount'];
        // target transfer user
        $userReceiver = $this->userRepository->findByEmail($attributes['email']);
        // current user
        DB::beginTransaction();
        try {
            // check balance before transfer
            if ($this->userRepository->find($this->currentUser->id)->balance >= $amountTransfer) {
                // create transaction data for sender
                $dataTransaction = [
                    'type_transaction_id' => $typeTransferId,
                    'amount' => $amountTransfer,
                    'previous_balance' => $this->currentUser->balance,
                    'notes' => $attributes['notes'] ?? '',
                    'direction_user_id' => $userReceiver->id
                ];
                $this->userTransactionRepository->create($this->currentUser->id, $dataTransaction, true);
                $this->userRepository->modifyBalance($this->currentUser->id, $amountTransfer, 'decrement');
                // create transaction data for receiver
                $dataTransaction = [
                    'type_transaction_id' => $typeReceiveId,
                    'amount' => $amountTransfer,
                    'previous_balance' => $userReceiver->balance,
                    'notes' => $attributes['notes'] ?? '',
                    'direction_user_id' => $this->currentUser->id
                ];
                $this->userTransactionRepository->create($userReceiver->id, $dataTransaction, true);
                $this->userRepository->modifyBalance($userReceiver->id, $amountTransfer, 'increment');
                DB::commit();
                return [
                    'status' => true,
                    'message' => 'Success Transfer'
                ];
            }
            Throw new \Exception('Balance not enough');
        } catch (\Throwable $th) {
            DB::rollBack();
            return [
                'status' => false,
                'message' => $th->getMessage()
            ];
        }
    }
}
