<?php
namespace App\Http\Usecase\Interfaces;

use Illuminate\Http\Request;

interface UserTransactionUsecaseInterface
{
    public function home();
    public function history();
    public function topup_index(Request $request);
    public function topup($attributes);
    public function transfer_index(Request $request);
    public function transfer($attributes);
}
