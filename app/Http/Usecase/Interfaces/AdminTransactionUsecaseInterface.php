<?php
namespace App\Http\Usecase\Interfaces;

use Illuminate\Http\Request;

interface AdminTransactionUsecaseInterface
{
    public function index();
    public function topup(Request $request);
    public function topup_confirmed($transactionId);
}
