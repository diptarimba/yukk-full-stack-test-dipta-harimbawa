<?php
namespace App\Http\Usecase\Interfaces;

interface UserUsecaseInterface
{
    public function index();
    public function find($id);
    public function store(array $attributes);
    public function update($id, array $attributes);
}
