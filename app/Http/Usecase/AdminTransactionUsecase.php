<?php

namespace App\Http\Usecase;

use App\Http\Repository\Interfaces\TypeTransactionRepositoryInterface;
use App\Http\Repository\Interfaces\UserRepositoryInterface;
use App\Http\Repository\Interfaces\UserTransactionRepositoryInterface;
use App\Http\Usecase\Interfaces\AdminTransactionUsecaseInterface;
use App\Http\Usecase\Traits\Colour;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AdminTransactionUsecase implements AdminTransactionUsecaseInterface
{
    use Colour;

    protected $userRepository;
    protected $typeTransactionRepository;
    protected $userTransactionRepository;
    public function __construct(
        UserRepositoryInterface $userRepository,
        TypeTransactionRepositoryInterface $typeTransactionRepository,
        UserTransactionRepositoryInterface $userTransactionRepository
    ) {
        $this->userRepository = $userRepository;
        $this->typeTransactionRepository = $typeTransactionRepository;
        $this->userTransactionRepository = $userTransactionRepository;
    }
    public function index()
    {
        $sumAllBalance = $this->userRepository->sumAllBalance();
        $countAllUser = $this->userRepository->countAllUser();
        return [
            'all_balance' => $sumAllBalance,
            'all_user' => $countAllUser
        ];
    }

    public function topup(Request $request)
    {
        $typeId = $this->typeTransactionRepository->find('topup')->id;
        $payload = $this->userTransactionRepository->all_transaction_type([$typeId], null, $request->search['value']);
        return datatables()->of($payload)
            ->addIndexColumn()
            ->addColumn('status', function ($data) {
                return $data->transaction_detail->is_approve ? 'Approve' : 'Pending';
            })
            ->addColumn('amount', function ($data) {
                return "Rp. " . number_format($data->amount, 0, ',', '.');
            })
            ->addColumn('proof', function ($data) {
                return '<a class="' . self::CLASS_BUTTON_PRIMARY . '" href="' . $data->transaction_detail->proof . '" target="_blank">View</a>';
            })
            ->addColumn('action', function ($data) {
                $disabled = $data->transaction_detail->is_approve ? 'disabled' : '';
                $colour = $data->transaction_detail->is_approve ? self::CLASS_BUTTON_SUCCESS : self::CLASS_BUTTON_DANGER;
                $text = $data->transaction_detail->is_approve ? 'Approved' : 'Approve';
                $ident = Str::random(10);
                $actionPath = route('admin.topup.store', $data->id);
                return '<button type="button" '.$disabled.'  onclick="confirm_button(\'form' . $ident . '\')"class="' . $colour . '">'.$text.'</button>' . '<form id="form' . $ident . '" action="' . $actionPath . '" method="post"> <input type="hidden" name="_token" value="' . csrf_token() . '" /> <input type="hidden" name="_method" value="PATCH"> </form>';
            })
            ->rawColumns(['action', 'proof'])
            ->make(true);
    }

    public function topup_confirmed($transactionId)
    {
        $transaction = $this->userTransactionRepository->find($transactionId);
        $this->userTransactionRepository->approve_topup($transactionId);
        $this->userRepository->modifyBalance($transaction->user_id, $transaction->amount, 'increment');

        return redirect()->route('admin.topup.index')->with('success', 'Topup Success');
    }
}
