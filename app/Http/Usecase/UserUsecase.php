<?php
namespace App\Http\Usecase;

use App\Http\Repository\Interfaces\UserRepositoryInterface;
use App\Http\Usecase\Interfaces\UserUsecaseInterface;
use Illuminate\Support\Facades\Hash;

class UserUsecase implements UserUsecaseInterface
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        return $this->userRepository->index();
    }

    public function find($id)
    {
        return $this->userRepository->find($id);
    }

    public function store($attributes)
    {
        $user = [
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'password' => Hash::make($attributes['password']),
        ];
        return $this->userRepository->store($user);
    }

    public function update($id, $attributes)
    {
        $existingUser = $this->userRepository->find($id);
        $attributes['password'] = $attributes['password'] ?? $existingUser->password;
        $attributes['password'] = empty($attributes['password']) ? $existingUser->password : Hash::make($attributes['password']);

        $attributes['name'] = $attributes['name'] ?? $existingUser->name;
        $attributes['email'] = $attributes['email'] ?? $existingUser->email;

        $user = [
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'password' => $attributes['password']
        ];
        return $this->userRepository->update($id, $user);
    }
}
