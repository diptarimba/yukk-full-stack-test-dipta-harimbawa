<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class RedirectIfNotSuitableRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = Auth::user();

        if ($user->hasRole('admin') && $request->is('user/*')) {
            return redirect('/dashboard');
        } elseif ($user->hasRole('user') && $request->is('admin/*')) {
            return redirect('/dashboard');
        }

        return $next($request);
    }
}
