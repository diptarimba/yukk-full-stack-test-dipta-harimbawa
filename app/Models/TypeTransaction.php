<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeTransaction extends Model
{
    use HasFactory, HasUuids;
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'need_approval', 'code', 'increment', 'conjunction'
    ];

    public function user_transaction()
    {
        return $this->hasMany(UserTransaction::class, 'type_transaction_id', 'id');
    }
}
