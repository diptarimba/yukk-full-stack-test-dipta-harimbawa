<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory, HasUuids;
    protected $primaryKey = 'id';

    protected $fillable = [
        'transaction_id', 'notes', 'proof', 'is_approve'
    ];

    public function transaction()
    {
        return $this->belongsTo(UserTransaction::class, 'transaction_id', 'id');
    }
}
