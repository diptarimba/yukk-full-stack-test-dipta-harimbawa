<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class UserTransaction extends Model
{
    use HasFactory, HasUuids;
    protected $primaryKey = 'id';


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->trx_id = now()->format('Ymd') . strtoupper(Str::random(10));
        });
    }


    protected $fillable = [
        'trx_id', 'user_id', 'direction_user_id', 'type_transaction_id', 'amount', 'previous_balance'
    ];

    public function type_transaction()
    {
        return $this->belongsTo(TypeTransaction::class, 'type_transaction_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function direction_user()
    {
        return $this->belongsTo(User::class, 'direction_user_id', 'id');
    }

    public function transaction_detail()
    {
        return $this->hasOne(TransactionDetail::class, 'transaction_id', 'id');
    }
}
