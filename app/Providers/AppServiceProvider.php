<?php

namespace App\Providers;

use App\Http\Repository\Interfaces\TypeTransactionRepositoryInterface;
use App\Http\Repository\Interfaces\UserRepositoryInterface;
use App\Http\Repository\Interfaces\UserTransactionRepositoryInterface;
use App\Http\Repository\TypeTransactionRepository;
use App\Http\Repository\UserRepository;
use App\Http\Repository\UserTransactionRepository;
use App\Http\Usecase\AdminTransactionUsecase;
use App\Http\Usecase\Interfaces\AdminTransactionUsecaseInterface;
use App\Http\Usecase\Interfaces\UserTransactionUsecaseInterface;
use App\Http\Usecase\Interfaces\UserUsecaseInterface;
use App\Http\Usecase\UserTransactionUsecase;
use App\Http\Usecase\UserUsecase;
use Illuminate\Support\ServiceProvider;
use Spatie\Permission\PermissionServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserUsecaseInterface::class, UserUsecase::class);
        $this->app->bind(AdminTransactionUsecaseInterface::class, AdminTransactionUsecase::class);
        $this->app->bind(UserTransactionUsecaseInterface::class, UserTransactionUsecase::class);
        $this->app->bind(UserTransactionRepositoryInterface::class, UserTransactionRepository::class);
        $this->app->bind(TypeTransactionRepositoryInterface::class, TypeTransactionRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
