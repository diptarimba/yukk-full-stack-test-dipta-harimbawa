@extends('layout.app')

@section('page-link', route('user.home'))
@section('page-title', 'User')
@section('sub-page-title', 'Transfer')

@section('content')
    <x-util.card title="User Transfer">
        <x-form.base url="{{ route('user.transfer.store') }}" method="POST">
            <x-form.input oninput="this.value = this.value.replace(/[^a-zA-Z0-9@_.]/g, '');" name="email" type="email" label="Email" placeholder="input the email" value="" />
            <x-form.input oninput="this.value = this.value.replace(/^[._]+|[._]+$|[^0-9_.]/g, '')" name="amount" type="number" label="Jumlah" placeholder="input the amount" value="" />
            <x-form.input name="notes" type="text" label="Keterangan" placeholder="input the notes" value="" />
            <x-button.submit />
            <x-button.cancel url="{{ route('user.home') }}" />
        </x-form.base>
    </x-util.card>
@endsection
