@extends('layout.app')


@section('page-link', route('user.home'))
@section('page-title', 'User')
@section('sub-page-title', 'Transfer')

@section('content')
    <x-util.card title="Daftar Transfer" add url="{{route('user.transfer')}}" custom-text="Transfer">
        <table id="datatable" class="table w-full pt-4 text-gray-700 dark:text-zinc-100 datatables-target-exec">
            <thead>
                <tr>
                    <th class="p-4 pr-8 border rtl:border-l-0 border-y-2 border-gray-50 dark:border-zinc-600">Id</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Trx Id</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Transaksi</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Dari/Ke</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Nominal</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Keterangan</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Status</th>
                    <th class="p-4 pr-8 border border-y-2 border-gray-50 dark:border-zinc-600 border-l-0">Waktu</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </x-util.card>
@endsection

@section('custom-footer')
<x-datatables.single url="{{route('user.transfer.index')}}">
    <x-datatables.column name="trx_id"/>
    <x-datatables.column name="type_transaction.name"/>
    <x-datatables.column name="direction_user_name"/>
    <x-datatables.column name="amount"/>
    <x-datatables.column name="transaction_detail.notes"/>
    <x-datatables.column name="status"/>
    <x-datatables.column name="created_at"/>
</x-datatables.single>
@endsection
