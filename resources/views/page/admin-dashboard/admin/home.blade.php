@extends('layout.app')

@section('page-link', '/')
@section('page-title', 'Dashboard')
@section('sub-page-title', 'Dashboard')

@section('content')
<div class="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-5">
    <x-home.card-mini title="Total Balance User" prefix="" counterValue="{{number_format($data['all_balance'], 0, ',', '.')}}" suffix="" color="" valueChanged="" information="Since last week"/>
    <x-home.card-mini title="Total User" prefix="" counterValue="{{$data['all_user']}}" suffix="" color="" valueChanged="" information="Since last week"/>
</div>
@endsection
