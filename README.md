# Tutorial Instalasi Project Laravel

Tutorial ini akan membimbing anda melalui proses instalasi project Laravel ke sistem lokal. Ikuti langkah-langkah di bawah ini untuk mengatur project ini dengan benar.

## Prasyarat

Sebelum memulai, pastikan anda sudah memiliki **PHP versi 8.2** terinstal di local. Anda bisa memeriksa versi PHP yang terinstal dengan menjalankan perintah ini di terminal:

```bash
php -v
```

## Langkah-Langkah Instalasi

Berikut adalah langkah-langkah untuk menginstall project:

1. **Clone Project ke Local Computer**

   Gunakan perintah ini untuk meng-clone repository:

   ```bash
   git clone https://gitlab.com/diptarimba/yukk-full-stack-test-dipta-harimbawa.git
   ```

2. **Instalasi Dependencies**

   Masuk ke direktori project dan jalankan:

   ```bash
   composer install
   ```

3. **Konfigurasi File Environment**

   Copy file `.env.example` ke `.env` dan sesuaikan konfigurasi sesuai dengan kebutuhan:

   ```bash
   cp .env.example .env
   ```

4. **Jalankan Docker Container dengan Laravel Sail**

   Pastikan Docker telah terinstal di sistem, kemudian eksekusi:

   ```bash
   sail up -d
   ```

5. **Inisialisasi Database dengan Seeder**

   Untuk mengisi database dengan data awal yang diperlukan, jalankan:

   ```bash
   sail artisan migrate:fresh --seed
   ```

## Flow App

![Design App](DesignApps.png)

## Tutorial Percobaan

Ikuti langkah-langkah ini untuk mencoba fungsionalitas dasar dari aplikasi ini:

1. **Registrasi Dua Akun**

   Buat dua akun pengguna melalui interface registrasi aplikasi.

2. **Top Up Request**

   Setiap akun melakukan request top up melalui interface yang disediakan.

3. **Login sebagai Admin**

   Gunakan akun admin untuk masuk ke aplikasi.

4. **Approve Top Up**

   Dari sidebar admin, pilih 'Top Up' dan approve kedua request top up yang telah dibuat.

5. **Transfer Saldo Antar Akun**

   Login kembali ke masing-masing akun pengguna dan lakukan transfer saldo satu sama lain.

Dengan mengikuti langkah-langkah ini, anda bisa melakukan percobaan aplikasi ini.
