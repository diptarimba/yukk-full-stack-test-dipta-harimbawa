<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\User\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return redirect()->intended(route('login', absolute: false));
});

Route::get('/dashboard', function () {
    if (Auth::user()->hasRole('admin')) {
        return redirect()->intended(route('admin.home', absolute: false));
    } else if (Auth::user()->hasRole('user')) {
        return redirect()->intended(route('user.home', absolute: false));
    }
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware(['auth', 'role.redirect'])->group(function () {

    Route::prefix('user')->as('user.')->middleware(['role:user'])->group(function () {
        Route::get('/home', [UserController::class, 'index'])->name('home');
        Route::get('/topup/index', [UserController::class, 'topup_index'])->name('topup.index');
        Route::get('/transfer/index', [UserController::class, 'transfer_index'])->name('transfer.index');
        Route::get('/transfer',[UserController::class, 'transfer'])->name('transfer');
        Route::post('/transfer',[UserController::class, 'transfer_store'])->name('transfer.store');
        Route::get('/topup', [UserController::class, 'topup'])->name('topup');
        Route::post('/topup', [UserController::class, 'topup_store'])->name('topup.store');
    });

    Route::prefix('admin')->as('admin.')->middleware(['role:admin'])->group(function () {
        Route::get('/home', [AdminController::class, 'index'])->name('home');
        Route::get('/topup', [AdminController::class, 'topup'])->name('topup.index');
        Route::patch('/topup/{id}/confirmed', [AdminController::class, 'topup_store'])->name('topup.store');
    });

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
