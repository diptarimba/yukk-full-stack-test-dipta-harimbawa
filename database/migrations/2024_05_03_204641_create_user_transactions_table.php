<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_transactions', function (Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->string('trx_id');
            $table->uuid('user_id');
            $table->uuid('direction_user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuid('type_transaction_id');
            $table->foreign('type_transaction_id')->references('id')->on('type_transactions')->onDelete('cascade');
            $table->bigInteger('amount');
            $table->bigInteger('previous_balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_transactions');
    }
};
