<?php

namespace Database\Seeders;

use App\Models\TypeTransaction;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TypeTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            [
                'name' => 'Top Up',
                'need_approval' => true,
                'code' => 'topup',
                'increment' => true,
            ],
            [
                'name' => 'Transfer',
                'need_approval' => false,
                'code' => 'transfer',
                'increment' => false,
                'conjunction' => 'ke'
            ],
            [
                'name' => 'Receive',
                'need_approval' => false,
                'code' => 'receive',
                'increment' => true,
                'conjunction' => 'dari'
            ]
        ];

        foreach($data as $each) {
            TypeTransaction::firstOrCreate($each);
        }
    }
}
